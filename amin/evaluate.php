<?php
 /*
	create database eval;
	use eval;
	create table person(
	id varchar(255) primary key,
	firstname varchar(255),
	lastname varchar(255)
	);
	create table result(
	  pid varchar(255),
	  qid int,
	  value int,
	  comment text
	);
	create table qkeys(
	  qid int primary key,
	  qkey text
	);
	insert into qkeys values('1','የትምህርትና ቴክኖሎጂ ልማት ሠራዊት እና የለውጥ መሣሪያዎችን (BPR, BSC, Kaizen) በመጠቀም ተግባርት እንዲፈጸሙ የመሪነት ሚናውን ከመጫወት አንጻር ');
	insert into qkeys values('2','ለዩኒቨርስቲው  ራእይ፤ ተልእኮ እና ግብ መሳካት ሰራተኞችን በማነሳሳት በአርአያነት ሊጠቀስ በሚችል  አመራር በስራ ክፍሉና በተቋሙ ልዩ ውጤት ከማስመዝገብ አንጻረ');
	insert into qkeys values('3','የውስብስብነት ባህሪ ያላቸዉን ስራዎች በቅልጥፍናና በመልካም ሁኔታ ማሰተዳደርና መምራት ');
	insert into qkeys values('4','ለሌሎች በቂ ጊዜ በመስጠት ሙያዊ  ክትትልና ድጋፍ በማድረግ ተቋማዊ የሆነ ችሎታን እና ተግባርን በማሻሻል ጥረት ማድረግ ውጤት ማስመዝገብ');
	insert into qkeys values('5','ከሁኔታዎች ጋር ራሱን በማዋሀድ ለሚነሱ ጥያቄዎች እንደየ ተጨባጭ ሁኔታቸው አግባብ መልስ ስለመስጠት');
	insert into qkeys values('6','የክፍሉን አላማ ለማሳካት አስተዋፅኦ የሚያበረክቱ ፕሮጀክቶችን መስራትና መምራት');
	insert into qkeys values('7','በስሩ የሚገኙ ሰራተኞች ክህሎት፤ እዉቀት እና ችሎታ ለማሻሻልና ጓዳዊ በሆነ ሁኔታ በመምራት የሚያደርገው ጥረት ');
	insert into qkeys values('8','በስራ ድርሻው ከሚጠበቅበት በላይ ከሌሎች ጋር በመስራት  የተለየ ገንቢ ሚና ስለመጫወቱ');
	insert into qkeys values('9','የስራ ክፍሉን አላማ ለማሳካት አስተዋፅኦ የሚያበረክቱ አዳዲስ አሰራሮችን በማመንጨት አዎንታዊ ለውጥ በማምጣት ለሌሎች ተሞክሮ ከማስተላለፍ አንጻር የተሠሩ ሥራዎች');
	insert into qkeys values('10','ክህሎትና ዕዉቀትን  በማሻሻል የሰራተኞችን የስራ አፈፃፀም በተከታታይነት እንዲሻሻል ማስቻል');
	insert into qkeys values('11','አርአያ ሊሆኑ የሚችሉ በጎ ባህሪያትንና ተግባራትን በማሳየት በስራ ክፍሉ አዎንታዊ ተፅዕኖ ማሳደር ');
	insert into qkeys values('12','አጠቃላይ ዕቅድ ዝግጀትና የፕሮግራም በጀት አፈጻጸምና ውጤታማነት መምራት');
	insert into qkeys values('13','የክንውን ዕቅድን ከበጀት ጋር ማቀናጀትና ውጤታማ የበጀት አጠቃቀም መፍጠር');
	insert into qkeys values('14','የውጤት ተኮር/ስትራቴጅክ ፕላን ዝግጅትና አፈፃፀም ውጤታማነት ማሻሻል ');
	insert into qkeys values('15','ከዩኒቨርሲቲው አስተዳደር ክፍሎች ለሚጠየቁ ጥያቄዎች ፈጣንና አግባብ ያለው ምላሽ መስጠት');
	insert into qkeys values('16','የመንግስት መመሪያዎችንና ደንቦችን በተገቢው የመስፈጸምና ሌሎችም እንዲያቋቸው ማድረግ');
	insert into qkeys values('17','ፍላጎትን መሰረት ያደረገ  የስርዓተ-ትምህርት ክለሳ አሰራር፤  አፈጻጸምና ክትትል አንጻር የፈጸመው ተግባር  ');
	insert into qkeys values('18','የተማሪዎች ምደባ ፍትሐዊነት ጋር ተያይዞ የሚነሱ ቅሬታዎችን ሰላማዊ በሆነ መንገድ በመፍታት፤መጠኔ-ምረቃን ለመጨመር፤  በውጤት ማነስና ሌሎች ምክንያቶች የሚፈጠረውን መጠነ ማቋረጥ(attrition) ለመቀነስ ስልት ቀይሶ ከመምራት አንጻር የተጫወተው ሚና');
	insert into qkeys values('19','የመምህራን የስራ አፈጻጸም ግምገማ ና የግብረ-መልስ ስርኣት ወቅታዊነት፤ ፍትሐዊነት፤ አግባብነትና ውጤታማነት ከማረጋገጥ አንጻር የተጫወተው ሚና ');
	insert into qkeys values('20','የመምህራን ቅጥር በስታፍ ዲቨሎፕመንት ፕላን መሰረት ማስፈጸም');
	insert into qkeys values('21','ፕሮግራሞችን የመቅረጽ፤ የማሻሻል፤የማስፀደቅ፤የመከታተልና የመገምገም ብሎም የክለሳ ስርዓት  አሰራር ውጤታማነት ማረጋገጥ');
	insert into qkeys values('22','የስርዓተ-ትምህርት ቀረጻ እና ትግበራ አፈጻጸም ውጤታማነት ለማረጋገጥ የደረገው ክትትል');
	insert into qkeys values('23','የትምህርት ጥራትን ለማስጠበቅና ለማረጋገጥ የተዘረጋ አሰራር ሥርዓትና ውጤታማነት አንጻር የሚየደርገው እንቅስቃሴና የመጣው ተጨባጭ ለውጥ');
	insert into qkeys values('24','በማስተማር ወይም በምርምር ዘርፍ ልዩ የሆነ ውጤት እና ግኝት');
	insert into qkeys values('25','በቴክኖሎጂ ላይ የተመሰረተ የመማር-ማስተማር ሂደትና የተገኘ ተጨባጭ ፈጠራ ወይም አዲስ ግኝት ለማረጋገጥ የተደረገ ጥረት');
	insert into qkeys values('26','የአካዳሚክ ፕሮግራሞች በታቀደላቸው የጊዜ ገደብ መጠናቀቅና እንዲጠናቀቁ ከማድረግ አንጻር የሚፈጽመው ተግባር ');
	insert into qkeys values('27','የትምህርትና ቴክኖሎጂ ልማት ሠራዊት አደረጃጀትና አተገባበር( የተማሪዎችና የመምህራን) ተግባራዊ መሆኑን በመከታተል ወደ ታለመለት ግብ እየደረሰ መሆኑን ማረጋገጥ ');
	insert into qkeys values('28','በሀገር ውስጥም ሆነ በውጭ ሀገር ከሚገኙ ከሌሎች ተቋማት ጋር በምርምር ዘርፍ የተደረገ ትብብር ');
	insert into qkeys values('29','በምርምር፤በማስተማር እንዲሁም በማህበረሰብ አገልግሎት ለላቁ ስራዎች ዕውቅና ለመስጠትና ለመሸለም በተዘረጋው ስርዓት መፈጸም ');
	insert into qkeys values('30','የአካባቢዉን ማህበረሰብ  አእምሮአዊ፤ አካባቢያዊ፤ ማህበራዊ እና ባህላዊ እድገቱን ለማነቃቃት የተደረገ ድጋፍ ');
	insert into qkeys values('31','የበጎ ፈቃደኝነት መንፈስና አስተሳሰብ ለማህበረሰብ አገልግሎት ተቋማዊ በሆነ መልኩ ለመስጠት የሚያደረው ጥረትና ተጨባጭ ተግባር ');
	insert into qkeys values('32','ዕውቀትን ለማስተላለፍ /ለማሸጋገር እና መረጃን ለማካፈል የሚያስችሉ አጋጣሚዎችን ተጠቅሞ ስትራቴጂያዊ አቅጣጫ በመከተል ለውጥ ማምጣት');
	insert into qkeys values('33','የውሥጣዊና ውጫዊ ደንበኛን ችግር ለመፍታት ከመደበኛ ሰራ በላይ ምሳሌ የሚሆን አገልግሎት ስለመስጠቱ');
	insert into qkeys values('34','ዩኒቨርስቲውን በሁሉም ዘርፍ የላቀ ለማድረግ በትጋት በመስራት የሚያጋጠሙ ተግዳሮቶችን በመወጣት የተመዘገበ ውጤት ');
	insert into qkeys values('35','በማንኛዉም ሁኔታ ቢሆን በቅንነት፤በታማኝነት፤ በግልፅነት እና በተጠያቂነት ስለመስራት');
 */
 if(isset($_POST['submit'])){
 	$eval='';
 	$comment='';
 	$id=$_POST['id'];
 	$firstname=$_POST['firstname'];
 	$lastname=$_POST['lastname'];
 	$con=mysqli_connect('localhost','root','','eval') or die(mysqli_error($con));
 	$query1="insert into person(id,firstname,lastname) values('$id','$firstname','$lastname')";
 	$result=mysqli_query($con,$query1) or die(mysqli_error($con));
 	for($i=1;$i<$i<35;$i++){
 		$eval[$i-1]=$_POST['input'.$i];
 		if(isset($_POST['comment'.$i]))
 		$comment[$i-1]=$_POST['comment'.$i];
 		else $comment[$i-1]=" ";
 		$query2="insert into result(pid,qid,value,comment) values('$id','$i','".$eval[$i-1]."','".$comment[$i-1]."')";
 		mysqli_query($con,$query2) or die(mysqli_error($con));
 	}
 	if($result!==false)
 		echo "<script>window.alert('You evaluated: $firstname $lastname succesfully');</script>";
 	else echo "<script>window.alert('Error: evaluation failed something goes wrong');</script>";
 	
 }
?>
