<html>
<body>
 <form action="evaluate.php">
  <table border='5'>
  	<tr><th>ተ.ቁ</th><th>የመስፈርት አካላይ</th><th>የብቃት መስፈርቶች</th><th colspan="6">መመዘኛ ደረጃዎች</th><th>አስተያየት</th></tr>
  	<tr><td rowspan="16">1</td><td rowspan="16">Demonstrated
Leadership
አርአያነት ያለው አመራር</td><td>የትምህርትና ቴክኖሎጂ ልማት ሠራዊት እና የለውጥ መሣሪያዎችን (BPR, BSC, Kaizen) በመጠቀም ተግባርት እንዲፈጸሙ የመሪነት ሚናውን ከመጫወት አንጻር </td><td><input type='radio' name='input1' value='0'/>አይ</td><td><input type='radio' name='input1' value='1'/>1</td><td><input type='radio' name='input1' value='2'/>2</td><td><input type='radio' name='input1' value='3'/>3</td><td><input type='radio' name='input1' value='4'/>4</td><td><input type='radio' name='input1' required value='5'/>5</td><td><input type='text' name='comment1'/></td></tr><tr> <td>ለዩኒቨርስቲው  ራእይ፤ ተልእኮ እና ግብ መሳካት ሰራተኞችን በማነሳሳት በአርአያነት ሊጠቀስ በሚችል  አመራር በስራ ክፍሉና በተቋሙ ልዩ ውጤት ከማስመዝገብ አንጻረ</td><td><input type='radio' name='input2' value='0'/>አይ</td><td><input type='radio' name='input2' value='1'/>1</td><td><input type='radio' name='input2' value='2'/>2</td><td><input type='radio' name='input2' value='3'/>3</td><td><input type='radio' name='input2' required value='4'/>4</td><td><input type='radio' name='input2' value='5'/>5</td><td><input type='text' name='comment2'/></td>
  	</tr>

<tr> <td>የውስብስብነት ባህሪ ያላቸዉን ስራዎች በቅልጥፍናና በመልካም ሁኔታ ማሰተዳደርና መምራት </td><td><input type='radio' name='input3' value='0'/>አይ</td><td><input type='radio' name='input3' value='1'/>1</td><td><input type='radio' name='input3' value='2'/>2</td><td><input type='radio' name='input3' value='3'/>3</td><td><input type='radio' name='input3' value='4'/>4</td><td><input type='radio' name='input3' required value='5'/>5</td><td><input type='text' name='comment3'/></td>
  	</tr>

  	<tr> <td>ለሌሎች በቂ ጊዜ በመስጠት ሙያዊ  ክትትልና ድጋፍ በማድረግ ተቋማዊ የሆነ ችሎታን እና ተግባርን በማሻሻል ጥረት ማድረግ ውጤት ማስመዝገብ</td><td><input type='radio' name='input4' value='0'/>አይ</td><td><input type='radio' name='input4' value='1'/>1</td><td><input type='radio' name='input4' value='2'/>2</td><td><input type='radio' name='input4' value='3'/>3</td><td><input type='radio' name='input4' value='4'/>4</td><td><input type='radio' name='input4' required value='5'/>5</td><td><input type='text' name='comment4'/></td>
  	</tr>

  	<tr> <td>ከሁኔታዎች ጋር ራሱን በማዋሀድ ለሚነሱ ጥያቄዎች እንደየ ተጨባጭ ሁኔታቸው አግባብ መልስ ስለመስጠት</td><td><input type='radio' name='input5' value='0'/>አይ</td><td><input type='radio' name='input5' value='1'/>1</td><td><input type='radio' name='input5' value='2'/>2</td><td><input type='radio' name='input5' value='3'/>3</td><td><input type='radio' name='input5' value='4'/>4</td><td><input type='radio' name='input5' value='5' required />5</td><td><input type='text' name='comment5'/></td>
  	</tr>

  	<tr> <td>የክፍሉን አላማ ለማሳካት አስተዋፅኦ የሚያበረክቱ ፕሮጀክቶችን መስራትና መምራት</td><td><input type='radio' name='input6' value='0'/>አይ</td><td><input type='radio' name='input6' value='1'/>1</td><td><input type='radio' name='input6' value='2'/>2</td><td><input type='radio' name='input6' value='3'/>3</td><td><input type='radio' name='input6' value='4'/>4</td><td><input type='radio' name='input6' value='5' required  />5</td><td><input type='text' name='comment6'/></td>
  	</tr>
  	<tr> <td>በስሩ የሚገኙ ሰራተኞች ክህሎት፤ እዉቀት እና ችሎታ ለማሻሻልና ጓዳዊ በሆነ ሁኔታ በመምራት የሚያደርገው ጥረት </td><td><input type='radio' name='input7' value='0'/>አይ</td><td><input type='radio' name='input7' value='1'/>1</td><td><input type='radio' name='input7' value='2'/>2</td><td><input type='radio' name='input7' value='3'/>3</td><td><input type='radio' name='input7' value='4'/>4</td><td><input type='radio' name='input7' value='5' required />5</td><td><input type='text' name='comment7'/></td>
  	</tr>
  	
  	<tr> <td>በስራ ድርሻው ከሚጠበቅበት በላይ ከሌሎች ጋር በመስራት  የተለየ ገንቢ ሚና ስለመጫወቱ </td><td><input type='radio' name='input8' value='0'/>አይ</td><td><input type='radio' name='input8' value='1'/>1</td><td><input type='radio' name='input8' value='2'/>2</td><td><input type='radio' name='input8' value='3'/>3</td><td><input type='radio' name='input8' value='4'/>4</td><td><input type='radio' required name='input8' value='5'/>5</td><td><input type='text' name='comment8'/></td>
  	</tr>

  	<tr> <td>የስራ ክፍሉን አላማ ለማሳካት አስተዋፅኦ የሚያበረክቱ አዳዲስ አሰራሮችን በማመንጨት አዎንታዊ ለውጥ በማምጣት ለሌሎች ተሞክሮ ከማስተላለፍ አንጻር የተሠሩ ሥራዎች </td><td><input type='radio' name='input9' value='0'/>አይ</td><td><input type='radio' name='input9' value='1'/>1</td><td><input type='radio' name='input9' value='2'/>2</td><td><input type='radio' name='input9' value='3'/>3</td><td><input type='radio' name='input9' value='4'/>4</td><td><input type='radio' required name='input9' value='5'/>5</td><td><input type='text' name='comment9'/></td>
  	</tr>

  	<tr> <td>ክህሎትና ዕዉቀትን  በማሻሻል የሰራተኞችን የስራ አፈፃፀም በተከታታይነት እንዲሻሻል ማስቻል</td><td><input type='radio' name='input10' value='0'/>አይ</td><td><input type='radio' name='input10' value='1'/>1</td><td><input type='radio' name='input10' value='2'/>2</td><td><input type='radio' name='input10' value='3'/>3</td><td><input type='radio' name='input10' value='4'/>4</td><td><input type='radio' name='input10' value='5' required  />5</td><td><input type='text' name='comment10'/></td>
  	</tr>
  	<tr> <td>አርአያ ሊሆኑ የሚችሉ በጎ ባህሪያትንና ተግባራትን በማሳየት በስራ ክፍሉ አዎንታዊ ተፅዕኖ ማሳደር </td><td><input type='radio' name='input11' value='0'/>አይ</td><td><input type='radio' name='input11' value='1'/>1</td><td><input type='radio' name='input11' value='2'/>2</td><td><input type='radio' name='input11' value='3'/>3</td><td><input type='radio' name='input11' value='4'/>4</td><td><input type='radio' name='input11' value='5' required />5</td><td><input type='text' name='comment11'/></td>
  	</tr>

  	<tr> <td>አጠቃላይ ዕቅድ ዝግጀትና የፕሮግራም በጀት አፈጻጸምና ውጤታማነት መምራት </td><td><input type='radio' name='input12' value='0'/>አይ</td><td><input type='radio' name='input12' value='1'/>1</td><td><input type='radio' name='input12' value='2'/>2</td><td><input type='radio' name='input12' value='3'/>3</td><td><input type='radio' name='input12' value='4'/>4</td><td><input type='radio' name='input12' value='5' required />5</td><td><input type='text' name='comment12'/></td>
  	</tr>

  	<tr> <td>የክንውን ዕቅድን ከበጀት ጋር ማቀናጀትና ውጤታማ የበጀት አጠቃቀም መፍጠር</td><td><input type='radio' name='input13' value='0'/>አይ</td><td><input type='radio' name='input13' value='1'/>1</td><td><input type='radio' name='input13' value='2'/>2</td><td><input type='radio' name='input13' value='3'/>3</td><td><input type='radio' name='input13' value='4'/>4</td><td><input type='radio' name='input13' value='5' required />5</td><td><input type='text' name='comment13'/></td>
  	</tr>

  	<tr> <td>የውጤት ተኮር/ስትራቴጅክ ፕላን ዝግጅትና አፈፃፀም ውጤታማነት ማሻሻል </td><td><input type='radio' name='input14' value='0'/>አይ</td><td><input type='radio' name='input14' value='1'/>1</td><td><input type='radio' name='input14' value='2'/>2</td><td><input type='radio' name='input14' value='3'/>3</td><td><input type='radio' name='input14' value='4'/>4</td><td><input type='radio' required name='input14' value='5'/>5</td><td><input type='text' name='comment14'/></td>
  	</tr>
  	<tr> <td>ከዩኒቨርሲቲው አስተዳደር ክፍሎች ለሚጠየቁ ጥያቄዎች ፈጣንና አግባብ ያለው ምላሽ መስጠት </td><td><input type='radio' name='input15' value='0'/>አይ</td><td><input type='radio' name='input15' value='1'/>1</td><td><input type='radio' name='input15' value='2'/>2</td><td><input type='radio' name='input15' value='3'/>3</td><td><input type='radio' name='input15' value='4'/>4</td><td><input type='radio' name='input15' value='5' required />5</td><td><input type='text' name='comment15'/></td>
  	</tr>

  	<tr> <td>የመንግስት መመሪያዎችንና ደንቦችን በተገቢው የመስፈጸምና ሌሎችም እንዲያቋቸው ማድረግ</td><td><input type='radio' name='input16' value='0'/>አይ</td><td><input type='radio' name='input16' value='1'/>1</td><td><input type='radio' name='input16' value='2'/>2</td><td><input type='radio' name='input16' value='3'/>3</td><td><input type='radio' name='input16' value='4'/>4</td><td><input type='radio' name='input16' value='5' required />5</td><td><input type='text' name='comment16'/></td>
  	</tr>

  	<tr><td rowspan="13">2</td><td rowspan="13">Teaching and
Research
መማር-ማስተማር እና ምርምር</td><td>ፍላጎትን መሰረት ያደረገ  የስርዓተ-ትምህርት ክለሳ አሰራር፤  አፈጻጸምና ክትትል አንጻር የፈጸመው ተግባር   </td><td><input type='radio' name='input17' value='0'/>አይ</td><td><input type='radio' name='input17' value='1'/>1</td><td><input type='radio' name='input17' value='2'/>2</td><td><input type='radio' name='input17' value='3'/>3</td><td><input type='radio' name='input17' value='4'/>4</td><td><input type='radio' name='input17' required value='5'/>5</td><td><input type='text' name='comment17'/></td></tr>

 <tr> <td>የተማሪዎች ምደባ ፍትሐዊነት ጋር ተያይዞ የሚነሱ ቅሬታዎችን ሰላማዊ በሆነ መንገድ በመፍታት፤መጠኔ-ምረቃን ለመጨመር፤  በውጤት ማነስና ሌሎች ምክንያቶች የሚፈጠረውን መጠነ ማቋረጥ(attrition) ለመቀነስ ስልት ቀይሶ ከመምራት አንጻር የተጫወተው ሚና</td><td><input type='radio' name='input18' value='0'/>አይ</td><td><input type='radio' name='input18' value='1'/>1</td><td><input type='radio' name='input18' value='2'/>2</td><td><input type='radio' name='input18' value='3'/>3</td><td><input type='radio' name='input18' value='4'/>4</td><td><input type='radio' name='input18' value='5' required />5</td><td><input type='text' name='comment18'/></td>
  	</tr>
  	<tr> <td>የመምህራን የስራ አፈጻጸም ግምገማ ና የግብረ-መልስ ስርኣት ወቅታዊነት፤ ፍትሐዊነት፤ አግባብነትና ውጤታማነት ከማረጋገጥ አንጻር የተጫወተው ሚና </td><td><input type='radio' name='input19' value='0'/>አይ</td><td><input type='radio' name='input19' value='1'/>1</td><td><input type='radio' name='input19' value='2'/>2</td><td><input type='radio' name='input19' value='3'/>3</td><td><input type='radio' name='input19' value='4'/>4</td><td><input type='radio' name='input19' value='5' required />5</td><td><input type='text' name='comment19'/></td>
  	</tr>

  	<tr> <td>የመምህራን ቅጥር በስታፍ ዲቨሎፕመንት ፕላን መሰረት ማስፈጸም </td><td><input type='radio' name='input20' value='0'/>አይ</td><td><input type='radio' name='input20' value='1'/>1</td><td><input type='radio' name='input20' value='2'/>2</td><td><input type='radio' name='input20' value='3'/>3</td><td><input type='radio' name='input20' value='4'/>4</td><td><input type='radio' name='input20' value='5' required />5</td><td><input type='text' name='comment20'/></td>
  	</tr>

  	<tr> <td>ፕሮግራሞችን የመቅረጽ፤ የማሻሻል፤የማስፀደቅ፤የመከታተልና የመገምገም ብሎም የክለሳ ስርዓት  አሰራር ውጤታማነት ማረጋገጥ </td><td><input type='radio' name='input21' value='0'/>አይ</td><td><input type='radio' name='input21' value='1'/>1</td><td><input type='radio' name='input21' value='2'/>2</td><td><input type='radio' name='input21' value='3'/>3</td><td><input type='radio' name='input21' value='4'/>4</td><td><input type='radio' name='input21' value='5' required />5</td><td><input type='text' name='comment21'/></td>
  	</tr>

  	<tr> <td>የስርዓተ-ትምህርት ቀረጻ እና ትግበራ አፈጻጸም ውጤታማነት ለማረጋገጥ የደረገው ክትትል</td><td><input type='radio' name='input22' value='0'/>አይ</td><td><input type='radio' name='input22' value='1'/>1</td><td><input type='radio' name='input22' value='2'/>2</td><td><input type='radio' name='input22' value='3'/>3</td><td><input type='radio' name='input22' value='4'/>4</td><td><input type='radio' name='input22' value='5' required />5</td><td><input type='text' name='comment22'/></td>
  	</tr>

  	<tr> <td>የትምህርት ጥራትን ለማስጠበቅና ለማረጋገጥ የተዘረጋ አሰራር ሥርዓትና ውጤታማነት አንጻር የሚየደርገው እንቅስቃሴና የመጣው ተጨባጭ ለውጥ</td><td><input type='radio' name='input23' value='0'/>አይ</td><td><input type='radio' name='input23' value='1'/>1</td><td><input type='radio' name='input23' value='2'/>2</td><td><input type='radio' name='input23' value='3'/>3</td><td><input type='radio' name='input23' value='4'/>4</td><td><input type='radio' name='input23' value='5' required />5</td><td><input type='text' name='comment23'/></td>
  	</tr>

  	<tr> <td>በማስተማር ወይም በምርምር ዘርፍ ልዩ የሆነ ውጤት እና ግኝት</td><td><input type='radio' name='input24' value='0'/>አይ</td><td><input type='radio' name='input24' value='1'/>1</td><td><input type='radio' name='input24' value='2'/>2</td><td><input type='radio' name='input24' value='3'/>3</td><td><input type='radio' name='input24' value='4'/>4</td><td><input type='radio' name='input24' required value='5'/>5</td><td><input type='text' name='comment24'/></td>
  	</tr>

  	<tr> <td>በቴክኖሎጂ ላይ የተመሰረተ የመማር-ማስተማር ሂደትና የተገኘ ተጨባጭ ፈጠራ ወይም አዲስ ግኝት ለማረጋገጥ የተደረገ ጥረት</td><td><input type='radio' name='input25' value='0'/>አይ</td><td><input type='radio' name='input25' value='1'/>1</td><td><input type='radio' name='input25' value='2'/>2</td><td><input type='radio' name='input25' value='3'/>3</td><td><input type='radio' name='input25' value='4'/>4</td><td><input type='radio' name='input25' value='5' required  />5</td><td><input type='text' name='comment25'/></td>
  	</tr>

  	<tr> <td>የአካዳሚክ ፕሮግራሞች በታቀደላቸው የጊዜ ገደብ መጠናቀቅና እንዲጠናቀቁ ከማድረግ አንጻር የሚፈጽመው ተግባር </td><td><input type='radio' name='input26' value='0'/>አይ</td><td><input type='radio' name='input26' value='1'/>1</td><td><input type='radio' name='input26' value='2'/>2</td><td><input type='radio' name='input26' value='3'/>3</td><td><input type='radio' name='input26' value='4'/>4</td><td><input type='radio' name='input26' value='5' required />5</td><td><input type='text' name='comment26'/></td>
  	</tr>

  	<tr> <td>የትምህርትና ቴክኖሎጂ ልማት ሠራዊት አደረጃጀትና አተገባበር( የተማሪዎችና የመምህራን) ተግባራዊ መሆኑን በመከታተል ወደ ታለመለት ግብ እየደረሰ መሆኑን ማረጋገጥ  </td><td><input type='radio' name='input27' value='0'/>አይ</td><td><input type='radio' name='input27' value='1'/>1</td><td><input type='radio' name='input27' value='2'/>2</td><td><input type='radio' name='input27' value='3'/>3</td><td><input type='radio' name='input27' value='4'/>4</td><td><input type='radio' name='input27' value='5' required />5</td><td><input type='text' name='comment27'/></td>
  	</tr>

  	<tr> <td>በሀገር ውስጥም ሆነ በውጭ ሀገር ከሚገኙ ከሌሎች ተቋማት ጋር በምርምር ዘርፍ የተደረገ ትብብር  </td><td><input type='radio' name='input28' value='0'/>አይ</td><td><input type='radio' name='input28' value='1'/>1</td><td><input type='radio' name='input28' value='2'/>2</td><td><input type='radio' name='input28' value='3'/>3</td><td><input type='radio' name='input28' value='4'/>4</td><td><input type='radio' name='input28' value='5' required />5</td><td><input type='text' name='comment28'/></td>
  	</tr>

  	<tr> <td>በምርምር፤በማስተማር እንዲሁም በማህበረሰብ አገልግሎት ለላቁ ስራዎች ዕውቅና ለመስጠትና ለመሸለም በተዘረጋው ስርዓት መፈጸም </td><td><input type='radio' name='input29' value='0'/>አይ</td><td><input type='radio' name='input29' value='1'/>1</td><td><input type='radio' name='input29' value='2'/>2</td><td><input type='radio' name='input29' value='3'/>3</td><td><input type='radio' name='input29' value='4'/>4</td><td><input type='radio' name='input29' value='5' required />5</td><td><input type='text' name='comment29'/></td>
  	</tr>

  	<tr><td rowspan="6">3</td><td rowspan="6">Community Service
የማህበረሰብ አገልግሎት</td><td>የአካባቢዉን ማህበረሰብ  አእምሮአዊ፤ አካባቢያዊ፤ ማህበራዊ እና ባህላዊ እድገቱን ለማነቃቃት የተደረገ ድጋፍ    </td><td><input type='radio' name='input30' value='0'/>አይ</td><td><input type='radio' name='input30' value='1'/>1</td><td><input type='radio' name='input30' value='2'/>2</td><td><input type='radio' name='input30' value='3'/>3</td><td><input type='radio' name='input30' value='4'/>4</td><td><input type='radio' name='input30' value='5' required />5</td><td><input type='text' name='comment30'/></td></tr>

<tr> <td>የበጎ ፈቃደኝነት መንፈስና አስተሳሰብ ለማህበረሰብ አገልግሎት ተቋማዊ በሆነ መልኩ ለመስጠት የሚያደረው ጥረትና ተጨባጭ ተግባር </td><td><input type='radio' name='input31' value='0'/>አይ</td><td><input type='radio' name='input31' value='1'/>1</td><td><input type='radio' name='input31' value='2'/>2</td><td><input type='radio' name='input31' value='3'/>3</td><td><input type='radio' name='input31' value='4'/>4</td><td><input type='radio' name='input31' value='5' required />5</td><td><input type='text' name='comment31'/></td>
  	</tr>

  	<tr> <td>ዕውቀትን ለማስተላለፍ /ለማሸጋገር እና መረጃን ለማካፈል የሚያስችሉ አጋጣሚዎችን ተጠቅሞ ስትራቴጂያዊ አቅጣጫ በመከተል ለውጥ ማምጣት </td><td><input type='radio' name='input32' value='0'/>አይ</td><td><input type='radio' name='input32' value='1'/>1</td><td><input type='radio' name='input32' value='2'/>2</td><td><input type='radio' name='input32' value='3'/>3</td><td><input type='radio' name='input32' value='4'/>4</td><td><input type='radio' name='input32' value='5' required />5</td><td><input type='text' name='comment32'/></td>
  	</tr>

  	<tr> <td>የውሥጣዊና ውጫዊ ደንበኛን ችግር ለመፍታት ከመደበኛ ሰራ በላይ ምሳሌ የሚሆን አገልግሎት ስለመስጠቱ</td><td><input type='radio' name='input33' value='0'/>አይ</td><td><input type='radio' name='input33' value='1'/>1</td><td><input type='radio' name='input33' value='2'/>2</td><td><input type='radio' name='input33' value='3'/>3</td><td><input type='radio' name='input33' value='4'/>4</td><td><input type='radio' name='input33' value='5' required />5</td><td><input type='text' name='comment33'/></td>
  	</tr>

  	<tr> <td>ዩኒቨርስቲውን በሁሉም ዘርፍ የላቀ ለማድረግ በትጋት በመስራት የሚያጋጠሙ ተግዳሮቶችን በመወጣት የተመዘገበ ውጤት </td><td><input type='radio' name='input34' value='0'/>አይ</td><td><input type='radio' name='input34' value='1'/>1</td><td><input type='radio' name='input34' value='2'/>2</td><td><input type='radio' name='input34' value='3'/>3</td><td><input type='radio' name='input34' value='4'/>4</td><td><input type='radio' name='input34' value='5'required/>5</td><td><input type='text' name='comment34'/></td>
  	</tr>

  	<tr> <td>በማንኛዉም ሁኔታ ቢሆን በቅንነት፤በታማኝነት፤ በግልፅነት እና በተጠያቂነት ስለመስራት </td><td><input type='radio' name='input35' value='0'/>አይ</td><td><input type='radio' name='input35' value='1'/>1</td><td><input type='radio' name='input35' value='2'/>2</td><td><input type='radio' name='input35' value='3'/>3</td><td><input type='radio' name='input35' value='4'/>4</td><td><input type='radio' name='input35' value='5' required />5</td><td><input type='text' name='comment35'/></td>
  	</tr>

  </table>
  <input style="font-size: 25;padding-right: 10;float: right;" type="reset" value="ሁሉን አጥፋ"/><input type="submit" value="አስገባ" style="font-size: 25;padding-right: 0;float: right;" />
 </form>
</body>
</html>