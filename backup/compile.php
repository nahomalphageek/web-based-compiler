<?php
 $code='';
 $lang='';
 if($_SERVER['REQUEST_METHOD']=='POST'){
     $code=$_POST['code'];
     $lang=$_POST['lang'];
     $file=saveCode($code,$lang);
     
     $result=compile($file,$lang);
     $result=str_replace("/var/www/html/upload/temp/", "", $result);
     echo $result;
 }
 function saveCode($code,$lang){
    $file_name=generateFileName($lang,$code);
    /*$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/temp/".$file_name,"wb");
    fwrite($fp,$code);
    fclose($fp);*/
    return $file_name;
 }
 function generateFileName($lang,$code){
    $file=tempnam("/var/www/html/upload/temp", "cod");
    $file_name=$file;
    switch ($lang) {
    	case 'cpp':
    		file_put_contents($file.'.cpp', $code);
    		unlink($file);
    		return $file.'.cpp';
    		break;
    	case 'c':
    		file_put_contents($file.'.c', $code);
    		unlink($file);
    		return $file.'.c';
    		break;
    	case 'java':
    		file_put_contents($file.'.java', $code);
    		unlink($file);
    		return $file.'.java';
    		break;
    }
    
    return $file_name;
 }
 function compileCode($path, $lang){
 	 $output="";
    $p=explode('.', $path);
    $bin=$p[0];
    switch($lang){
        
        case 'cpp':
          $bin=$bin.".out";
          $output=shell_exec("g++ -ansi -pedantic -Wall -Wextra -Weffc++ -o $bin ".$path." 2>&1");
          break;
        case 'java':
          $bin=$bin.".java";
          $output=shell_exec("javac ".$path);
          break;
        case 'c':
          $bin=$bin.".out";
          $output=shell_exec("gcc -std=c99 -pedantic -Wall -Wshadow -Wpointer-arith -Wcast-qual -Wmissing-prototypes -o $bin ".$path." 2>&1");
        case 'python':
          $bin=$bin.".py";
    }
    return $output;
 }
 function compile($path, $lang){
     $output="";
    $p=explode('.', $path);
    $bin=$p[0];
    switch($lang){
        case 'cpp':
          $bin=$bin.".out";
          $output=shell_exec("g++ -ansi -pedantic -Wall -Wextra -Weffc++ -o $bin ".$path." 2>&1");
          break;
        case 'java':
          $bin=$bin.".java";
          $output=shell_exec("javac ".$path);
          break;
        case 'c':
          $bin=$bin.".out";
          $output=shell_exec("gcc -std=c99 -pedantic -Wall -Wshadow -Wpointer-arith -Wcast-qual -Wmissing-prototypes -o $bin ".$path." 2>&1");
        case 'python':
          $bin=$bin.".py";
    }
    return $output;
 }
?>