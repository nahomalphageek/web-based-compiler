<?php 
session_start();
$_SESSION['cmd']='';
$_SESSION['pipe']='';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>EditArea - the code editor in a textarea</title>
	<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
	<script language="Javascript" type="text/javascript" src="lib/jquery.js"></script>
	<script language="Javascript" type="text/javascript">
		// initialisation
		editAreaLoader.init({
			id: "editor_1"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,language: "en"
			,syntax: "cpp"	
		});
		editAreaLoader.init({
			id: "console"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,language: "en"
			,syntax: "c"	
		});
		
		// callback functions
		function my_save(id, content){
			alert("Here is the content of the EditArea '"+ id +"' as received by the save callback function:\n"+content);
		}
		
		function my_load(id){
			editAreaLoader.setValue(id, "The content is loaded from the load_callback function into EditArea");
		}
		
		function test_setSelectionRange(id){
			editAreaLoader.setSelectionRange(id, 100, 150);
		}
		
		function test_getSelectionRange(id){
			var sel =editAreaLoader.getSelectionRange(id);
			alert("start: "+sel["start"]+"\nend: "+sel["end"]); 
		}
		
		function test_setSelectedText(id){
			text= "[REPLACED SELECTION]"; 
			editAreaLoader.setSelectedText(id, text);
		}
		
		function test_getSelectedText(id){
			alert(editAreaLoader.getSelectedText(id)); 
		}
		
		function editAreaLoaded(id){
			if(id=="example_2")
			{
				open_file1();
				open_file2();
			}
		}
		
		function open_file1()
		{
			var new_file= {id: "to\\ é # € to", text: "$authors= array();\n$news= array();", syntax: 'php', title: 'beautiful title'};
			editAreaLoader.openFile('example_2', new_file);
		}
		
		function open_file2()
		{
			var new_file= {id: "Filename", text: "<a href=\"toto\">\n\tbouh\n</a>\n<!-- it's a comment -->", syntax: 'html'};
			editAreaLoader.openFile('example_2', new_file);
		}
		
		function close_file1()
		{
			editAreaLoader.closeFile('example_2', "to\\ é # € to");
		}
		
		function toogle_editable(id)
		{
			editAreaLoader.execCommand(id, 'set_editable', !editAreaLoader.execCommand(id, 'is_editable'));
		}
		function compile(){
			var code=editAreaLoader.getValue("editor_1");
			var lang=$("#language option:selected" ).attr("value");
			$.post("compile.php", { code:code, lang:lang } ,function(data){
            editAreaLoader.setValue("console",data);
            if(data=="" || data==" ")
            	editAreaLoader.setValue("console","Compiled Succesfully, 0 ERRORS, 0 WARNINGS");
        });
			
		}
		function run(){
			var code=editAreaLoader.getValue("editor_1");
			var input=$("#input").val();
			var lang=$("#language option:selected" ).attr("value");
			$.post("execute.php", { code:code, lang:lang, input:input } ,function(data){
            editAreaLoader.setValue("console",data);
            
        });
		}
		function save(){

		}
	
	</script>
</head>
<body>
<h2>EditArea examples</h2>

<form action='' method='post'>
	<fieldset>
		<legend>Code Editor</legend>
		<p>Test in English with php syntax, highlighted, toggle enabled, word-wrap enabled, resize enabled and default toolbar. Also offer the possibility to switch on/off the readonly mode.</p>
		<textarea id="editor_1" style="height: 350px; width: 100%;" name="test_1">
            #include <iostream>
            using namespace std;
            int main(){
                cout <<"Hello World"<<endl;
                return 0;
            }
		</textarea>
		<p>Input: <input type="text" id="input" placeholder="Enter STDIN inputs separated by comma, or leave empty if not needed" size="60"/></p>
		<p>Command</p>
		   <select id="language">
			   <option value="cpp" selected="true">C++</option>
			   <option value="c">C</option>
			   <option value="java">java</option>
		   </select>
		   <input type='button' onclick="compile()" value="Compile"/>
		   <input type='button' onclick="run()" value="run"/>
		   <input type='button' onclick="save()" value="save"/>
	</fieldset>
</form>
<form action="" method="">
	<textarea id="console" style="height: 350px; width: 100%;" name="test_1">
		#output will show up here
	</textarea>
</form>
</body>
</html>
