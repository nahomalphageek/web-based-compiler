<?php
session_start();
//if($_SESSION['captcha']!=true)
//header("Location: temp.php");

?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AMU &mdash; Online compiler project</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
	<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
	<script language="Javascript" type="text/javascript" src="lib/jquery.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/spinner.css">
	<link rel="stylesheet" href="dist/css/slidePanel.css">
	<script src="js/toc.js"></script>
	<script src="js/prism.js"></script>
	<script src="dist/jquery-slidePanel.js"></script>
	

	<script language="Javascript" type="text/javascript">
		$body = $("body");

		$(document).on({
		    ajaxStart: function() { $body.addClass("loading");    },
		     ajaxStop: function() { $body.removeClass("loading");
		     }    
		});
		$(document).on("click",function(){
			$body.addClass("loading");
		});
		(function() {
			$('#toc').toc();
		})();
		function openModal() {
        $("#loading").show();
		}
		function hideModal(){
			$("#loading").hide();
		}

		function closeModal() {
		    document.getElementById('modal').style.display = 'none';
		    document.getElementById('fade').style.display = 'none';
		}
		$(document).ready(function() {
			$('.example-close').on('click', function() {
				$.slidePanel.show({
					content: '<div><h2>Title</h2><p>content here</p><a href="#" class="close">click to close</a></div>'
				}, {
					closeSelector: '.close'
				});
			});
		});
		// initialisation
		editAreaLoader.init({
			id: "editor_1"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,language: "en"
			,syntax: "cpp"	
		});
		editAreaLoader.init({
			id: "console"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,language: "en"
			,syntax: "c"	
		});
		
		// callback functions
		function my_save(id, content){
			alert("Here is the content of the EditArea '"+ id +"' as received by the save callback function:\n"+content);
		}
		
		function my_load(id){
			editAreaLoader.setValue(id, "The content is loaded from the load_callback function into EditArea");
		}
		
		function test_setSelectionRange(id){
			editAreaLoader.setSelectionRange(id, 100, 150);
		}
		
		function test_getSelectionRange(id){
			var sel =editAreaLoader.getSelectionRange(id);
			alert("start: "+sel["start"]+"\nend: "+sel["end"]); 
		}
		
		function test_setSelectedText(id){
			text= "[REPLACED SELECTION]"; 
			editAreaLoader.setSelectedText(id, text);
		}
		
		function test_getSelectedText(id){
			alert(editAreaLoader.getSelectedText(id)); 
		}
		
		function editAreaLoaded(id){
			if(id=="example_2")
			{
				open_file1();
				open_file2();
			}
		}
		
		function open_file1()
		{
			var new_file= {id: "to\\ é # € to", text: "$authors= array();\n$news= array();", syntax: 'php', title: 'beautiful title'};
			editAreaLoader.openFile('example_2', new_file);
		}
		
		function open_file2()
		{
			var new_file= {id: "Filename", text: "<a href=\"toto\">\n\tbouh\n</a>\n<!-- it's a comment -->", syntax: 'html'};
			editAreaLoader.openFile('example_2', new_file);
		}
		
		function close_file1()
		{
			editAreaLoader.closeFile('example_2', "to\\ é # € to");
		}
		
		function toogle_editable(id)
		{
			editAreaLoader.execCommand(id, 'set_editable', !editAreaLoader.execCommand(id, 'is_editable'));
		}
		function compile(){
			var code=editAreaLoader.getValue("editor_1");
			var lang=$("#language option:selected" ).attr("value");
			$.post("compile.php", { code:code, lang:lang } ,function(data){
            editAreaLoader.setValue("console",data);
            if(data=="" || data==" ")
            	editAreaLoader.setValue("console","Compiled Succesfully, 0 ERRORS, 0 WARNINGS");
        });
		}
		function validateSTDIN(code,lang,input){
			if(input=="" && ((code.indexOf("cin")>0) || (code.indexOf("scanf")>0) || (code.indexOf("nextLine()")>0) ))
				return false;
		   return true;
		}
		function compile(){
			openModal();
			var code=editAreaLoader.getValue("editor_1");
			var lang=$("#language option:selected" ).attr("value");
			$.post("compile.php", { code:code, lang:lang } ,function(data){
            editAreaLoader.setValue("console",data);
            if(data=="" || data==" "){
            	editAreaLoader.setValue("console","Compiled Succesfully, 0 ERRORS, 0 WARNINGS");
            }
            hideModal();
        });
		}
		function run(){
			openModal();
			var code=editAreaLoader.getValue("editor_1");
			var input=$("#input").val();
			var lang=$("#language option:selected" ).attr("value");

			if(validateSTDIN(code,lang,input)==false){
				window.alert("Your code requires input from console, type your inputs in textbox below separated by comma");
				return false;
			}
			$.post("execute.php", { code:code, lang:lang, input:input } ,function(data){
            editAreaLoader.setValue("console",data);
            hideModal();
            
        });
		}
		$( document ).ready(function() {
		    hideModal();
		});
	
	
	</script>
	<style>
	/* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
#fade {
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

#modal2 {
    display: block;
    position: absolute;
    top: 45%;
    left: 45%;
    width: 64px;
    height: 64px;
    padding:30px 15px 0px;
    border: 3px solid #ababab;
    box-shadow:1px 1px 10px #ababab;
    border-radius:20px;
    background-color: white;
    z-index: 1002;
    text-align:center;
    overflow: auto;
}
</style>
	</head>
	<body>
		<div id="fade"></div>
        <div id="modal2">
            <img id="loader" src="images/loading.gif" />
        </div>

	<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			<div class="row">
				<div class="col-sm-2 col-xs-12">
					<div id="gtco-logo"><a href="index.html">Online Compiler <em>.</em></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1 main-nav">
					<ul>
						<li class="active"><a href="#" data-nav-section="home">Home</a></li>
						<li><a href="#" data-nav-section="features">Features</a></li>
						<li><a href="#" data-nav-section="how-it-works">How It Works</a></li>
						<li><a href="#" data-nav-section="faq">FAQ</a></li>
						<li><a href="#" data-nav-section="products">Products</a></li>
						<li><a href="#" data-nav-section="contact">Contact</a></li>
						<li><a href="#" class="external" title="You will be navigated to the external pages">Login</a></li>
						<li class="btn-cta"><a href="#" class="external" title="You will be navigated to the external pages"><span>Sign up for free</span></a></li>
						<li><button class="example-close">close</button></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<div id="gtco-header" data-section="home" class="gtco-cover header-fixed" role="banner" style="background-image:url(images/background1.jpg);">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					<div class="display-t">
						<div class="display-tc">
							<h1 class="animate-box" data-animate-effect="fadeIn">Online Compiler Project</h1>
							<h2 class="animate-box" data-animate-effect="fadeIn">Compile C++, Java and C online now <em>powered by</em> <a href="#" target="_blank">Arbaminch University</a></h2>
							<p class="animate-box" data-animate-effect="fadeIn"><a href="#" class="btn btn-primary btn-lg">Sign Up for Free</a></p>
							<p class="gtco-video-link animate-box" data-animate-effect="fadeIn"><a href="https://www.youtube.com/user/nahometete/" class="popup-vimeo"><i class="icon-controller-play"></i> Watch the Video</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div id="gtco-features" class="border-bottom" data-section="features">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Online Features</h2>
					<p>Our free online code editor enables you to type your code on any browser. You can 
					also compile and run supported programming languages. </p>
				</div>
			</div>
			<div>
				<form action='' method='post'>
	<fieldset>
		<legend>Code Editor</legend>
		<p>Use this code editor to type in your code. You can compile the code using the buttons right below the editor.</p>
		<textarea id="editor_1" style="height: 350px; width: 100%;" name="test_1">
            #include <iostream>
            using namespace std;
            int main(){
                cout <<"Hello World"<<endl;
                return 0;
            }
		</textarea>
		<p>Input: <input type="text" id="input" placeholder="Enter STDIN inputs separated by comma, or leave empty if not needed" size="60"/></p>
		<p>Command</p>
		   <select id="language">
			   <option value="cpp">C++</option>
			   <option value="c">C</option>
			   <option value="java">java</option>
		   </select>
		   <input type='button' onclick="compile()" value="Compile"/>
		   <input type='button' onclick="run()" value="run"/>
		   <input type='button' onclick="test()" value="save"/>
		   <div id="loading"><img src="images/loading.gif"/>
		   		<h3>Please wait ... while we are working on it.</h3> 
		   </div>
	</fieldset>
</form>
<form action="" method="">
	<textarea id="console" style="height: 350px; width: 100%;" name="test_1">
		#output will show up here
	</textarea>
	<div id="code-snippets">
      <form>
      	<textarea rows="5" cols="40">These are code snippets submitted by students</textarea>
      </form>
	</div>
</form>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-vector"></i>
						</span>
						<h3>C Compiler</h3>
						<p>Compile C code using gcc on our ubuntu server. You can also run executables after compiling.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-tablet"></i>
						</span>
						<h3>C++ Compiler</h3>
						<p>Compile C++ online using GNU-G++ compiler. You can also run executable after compiling </p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-settings"></i>
						</span>
						<h3>Java Compiler</h3>
						<p>Compile java code to byte code using our online javac compiler. Currently we support only console java applications.. </p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-ruler-pencil"></i>
						</span>
						<h3>Web Design</h3>
						<p>This feature is coming soon. </p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-ticket"></i>
						</span>
						<h3>Code snippets</h3>
						<p>Access thousands of code snippets online. signup and start coding now! </p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-shield"></i>
						</span>
						<h3>High Security</h3>
						<p>We keep you private and secure. You don't need to wory about the security aspects.. </p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-gift"></i>
						</span>
						<h3>100% Free</h3>
						<p>No need to subscribe, just use our service for free. </p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="gtco-section border-bottom" id="how-it-works" data-section="how-it-works">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-6">

					<div class="gtco-heading">
						<h2 class="gtco-left">How It Works</h2>
						<p>Your code is compiled and executed on our dedicated Ubuntu servers. Then the result or output is sent through http/s to your browser.</p>
					</div>

					<div class="row">

						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-check"></i>
								</span>
								<div class="feature-copy">
									<h3>Code Editor</h3>
									<p>Built in code editor with syntax highlighting and autocomplete features is comming soon.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-check"></i>
								</span>
								<div class="feature-copy">
									<h3>Compile Online</h3>
									<p>No need to install compilers on your machine. We compile your code on one of our servers and send the results back to your browser.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-check"></i>
								</span>
								<div class="feature-copy">
									<h3>Run Executable</h3>
									<p>You can also run executable objects and get the output through your browser. We only support console applications</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-check"></i>
								</span>
								<div class="feature-copy">
									<h3>Save your work</h3>
									<p>You can also download your code as a source file for a later use.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p><a href="#" class="btn btn-primary btn-lg btn-block"><i class="icon-apple"></i> Download our mobile App </a></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 animate-box" data-animate-effect="fadeInRight">
					<img src="images/android1.png" alt="Free HTML5 Bootstrap Template" class="img-responsive">
				</div>
			</div>
		</div>
	</div>

	

	<div class="gtco-section" id="gtco-faq" data-section="faq">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Frequently Asked Questions</h2>
					<p>Any Questions? We have a huge AMU community who are willing to answer your questions. These are the most frequently asked questions till today.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">

					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>What is Online Compiler?</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>Online compiler is a web based solution of local compiler for programming languages. You can compile code through your browser, no need of installing compiler programs.</p>
							</div>
						</div>
					</div>
					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>How do I run GUI based programs</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>Our web app currently supports only console based applications, so you can not run Gui based programs.</p>
							</div>
						</div>
					</div>
					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>How do I use Online Compiler features?</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>All you have to do is type your code in the editor, compile, then you can run your program if you want.</p>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6">

					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>What programming language are available?</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>We only support C, C++, Java and Python languages. But we may add additional languages soon.</p>
							</div>
						</div>
					</div>
					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>Can I have a username that is already taken?</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>You can only create your account once, and you can't use that username for another account.</p>
							</div>
						</div>
					</div>
					<div class="gtco-accordion">
						<div class="gtco-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>Is Online Compiler free?</h3>
						</div>
						<div class="gtco-accordion-content">
							<div class="inner">
								<p>Yes, Online compiler is free for use in AMU Community.</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<div id="gtco-counter" class="gtco-section">
		<div class="gtco-container">

			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Mobile App</h2>
					<p>Our Android Application is comming soon, which enables you to edit code online as well as execute and save code right from your phone.</p>
				</div>
			</div>

			<div class="row">
				
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-download"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="00001" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Downloads</span>

					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-face-smile"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="000002" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Happy Clients</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-briefcase"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="40" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Compilations Done</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-time"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="00025" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Hours Spent</span>

					</div>
				</div>
					
			</div>
		</div>
	</div>

	<div id="gtco-products" data-section="products">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Products</h2>
					<p>We will post here when new products are available..</p>
				</div>
			</div>
			<div class="row">
				<div class="owl-carousel owl-carousel-carousel">
					<div class="item">
						<img src="images/img_1.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="images/img_2.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="images/img_3.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="images/img_4.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Subscribe</h2>
					<p>Be the first to know about our Online Compiler Status.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="form-inline">
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Your Email">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label for="name" class="sr-only">Name</label>
								<input type="text" class="form-control" id="name" placeholder="Your Name">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<button type="submit" class="btn btn-danger btn-block">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div id="gtco-contact" data-section="contact" class="gtco-cover gtco-cover-xs" style="background-image:url(images/background3.gif);">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row text-center">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-12">
							<h3>If you have inqueries please email us at <a href="#">info@amu.edu.et</a></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<footer id="gtco-footer" role="contentinfo">
		<div class="gtco-container">
			<div class="row row-pb-md">

				<div class="col-md-4">
					<div class="gtco-widget">
						<h3>About Us</h3>
						<p>Description about Arbaminch University ....</p>
					</div>
				</div>

				<div class="col-md-4 col-md-push-1">
					<div class="gtco-widget">
						<h3>Links</h3>
						<ul class="gtco-footer-links">
							<li><a href="#">Knowledge Base</a></li>
							<li><a href="#">Career</a></li>
							<li><a href="#">Press</a></li>
							<li><a href="#">Terms of services</a></li>
							<li><a href="#">Privacy Policy</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-4">
					<div class="gtco-widget">
						<h3>Get In Touch</h3>
						<ul class="gtco-quick-contact">
							<li><a href="#"><i class="icon-phone"></i> +251 965 290 133</a></li>
							<li><a href="#"><i class="icon-mail2"></i> info@amu.edu.et</a></li>
							<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>
						</ul>
					</div>
				</div>

			</div>

			<div class="row copyright">
				<div class="col-md-12">
					<p class="pull-left">
						<small class="block">&copy; 2017 Arbaminch University. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="http://www.facebook.com/animew.kerie" target="_blank">Animew Kerie</a> Images: <a href="http://bitbucket.com/" target="_blank">Bit Bucket</a></small>
					</p>
					<p class="pull-right">
						<ul class="gtco-social-icons pull-right">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>
	<div class="modal"><!-- Place at bottom of page --></div>
	
	</body>
</html>

